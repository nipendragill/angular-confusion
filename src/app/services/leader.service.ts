import { Injectable } from '@angular/core';
import { Leader } from '../shared/Leader';
import { LEADERS } from '../shared/Leaders';
import { resolve } from 'q';
import 'rxjs/add/observable/of';
import { Observable } from 'rxjs/Observable';
@Injectable()
export class LeaderService {

  constructor() { }
 
  getLeaders() : Observable<Leader[]>{
    return Observable.of(LEADERS).delay(2000);
  }


  getFeaturedLeader(): Observable<Leader>{
    return Observable.of(LEADERS.filter((Leader) => Leader.featured)[0]).delay(2000);
  }
}

